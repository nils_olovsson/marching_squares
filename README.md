**Marching squares**
A python implementation of the marching squares method for extracting iso
contour polygons from 2D scalar functions samples as a digital image.

Running the script will create the figures used in the blog post.

Blog post:
http://www.all-systems-phenomenal.com/articles/marching_squares

**License: MIT**
