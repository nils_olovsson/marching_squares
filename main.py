#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-04-XX
license: MIT

"""

import os
import sys

import numpy as np
import scipy
import skimage.io
import skimage.filters

from scipy import sparse
from scipy.sparse import linalg

import matplotlib.pyplot as plt

from matplotlib import rc
from matplotlib import animation

import MarchingSquares
import MetaBalls
import DrawPolygon
import Colors
import MakeIllustrations

# ==============================================================================
#
# 2D optimizable hat like function used as example
#
# ==============================================================================

cm = plt.cm.coolwarm

#delta = 0.01
delta = 0.05
#x = y = np.arange(-4.0, 4.00, delta)
#X, Y = np.meshgrid(x, y)

s = 0.75
t = 1

def genGrid(delta):
    """
        Generate the X, Y grid for 2D and 3D plots
    """
    x = y = np.arange(-4.0, 4.0, delta)
    X, Y = np.meshgrid(x, y)
    return X, Y, x, y

def hat(p):
    """
        A DoG hat function that grows far from origo, evaluated at a point
    """
    x = p[0]
    y = p[1]
    z1 = np.exp( -(x*x+y*y)/(2*t*t) )
    z2 = np.exp( -(x*x+y*y)/(2*s*s) )
    z3 = (0.10*x)**2 + (0.10*y)**2
    return z1 - z2 + z3

def hatGrid(X, Y):
    """
        A DoG hat function that grows far from origo, evaluated on a grid
    """
    Z1 = np.exp(-(X**2+Y**2)/(2*t*t))
    Z2 = np.exp(-(X**2+Y**2)/(2*s*s))
    Z3 = (0.10*X)**2 + (0.10*Y)**2
    Z = Z1 - Z2 + Z3
    return Z


# ==============================================================================
#
# Plot animated metaballs (not using matplotlib.animation)
#
# ==============================================================================

def getFilesInDirectory(directory, ftype=''):
    """
        Get all files of certain extension inside directory.
    """
    files = []
    try:
        for entry in os.listdir(directory):
            filepath = os.path.join(directory, entry)
            _,  ext = os.path.splitext(filepath)
            if os.path.isfile(filepath) and (len(ftype)==0 or ftype==ext):
                files.append(filepath)
    except:
        print(f"Failed to get files in {directory}")
        pass
    return files

def fadeToWhite(img, t):

    dtype = img.dtype
    faded = np.copy(img)
    faded = faded.astype(float)

    faded += t*255.0
    mask = faded > 255.0
    faded[mask] = 255.0
    faded = faded.astype(dtype)

    return faded

def fadeFromWhite(img, t):
    return fadeToWhite(img, 1.0-t)

def animateMetaballsImages():
    """
    """

    # ------------------------------------------------------
    # Animate and save images of metaballs
    # ------------------------------------------------------
    metaballs = MetaBalls.MetaBalls(8)
    img = metaballs.eval()
   
    colors = Colors.colors
    
    total_time = 20
    samples_per_second = 60
    fps    = samples_per_second
    frames = fps * total_time

    border = {}
    border['verts'] = 200*np.array([[0,1], [1,1], [1,0], [0,0]], dtype=float)
    border['edges'] = np.array([[0,1], [1,2], [2,3], [3,0]], dtype=int)
    bord_options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 3, 'width': 8.0}
    poly_options = {'draw-polygon': True, 'draw-faces': True}

    directory = './animation/'
    """
    for i in range(frames):
        print(f"Frame {i+1} of {frames}")
        metaballs.animate()
        img = metaballs.eval()
        iso = 2.0*np.mean(img)

        # Draw Marcing squares
        polygons, vertices, vindices = MarchingSquares.marchingSquares(img, iso, interpolate=False)
        dpi = 100
        fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
        ax = fig.gca()
        ax.set(xlim=(0, 1), ylim=(0, 1))
        for poly in polygons:
            fig, _, filled = DrawPolygon.filledLines(vertices, poly, color=colors['blue'], fig=fig, options=poly_options)
        fig, _, filled = DrawPolygon.filledLines(border['verts'], border['edges'], color='black', fig=fig, options=bord_options)
        ax = fig.gca()

        # These limits depend on the grid size of the metaballs since the
        # marching squares vertices are in image space...
        ax.set(xlim=(-10, 210), ylim=(-10, 210))

        plt.draw()
        filepath = os.path.join(directory, f'marching-squares-demo-1{i:04d}.png')
        plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
        plt.close()
    """

    # ------------------------------------------------------
    # Fade and copy images in order to create video
    # ------------------------------------------------------
    """
    files = getFilesInDirectory(directory)

    nr_fades = 4*samples_per_second
    dt = 1 / nr_fades

    # the video: last->white->first
    last = skimage.io.imread(files[-1], as_gray=False)
    t = 0.0
    for i in range(nr_fades):
        t += dt
        faded = fadeToWhite(last, t)
        filepath = os.path.join(directory, f'marching-squares-demo-2{i:04d}.png')
        skimage.io.imsave(filepath, faded, check_contrast=False)

    # Load first and last saved image and create set of faded images to fade
    first = skimage.io.imread(files[0], as_gray=False)
    t = 0.0
    for i in range(nr_fades):
        t += dt
        faded = fadeFromWhite(first, t)
        filepath = os.path.join(directory, f'marching-squares-demo-3{i:04d}.png')
        skimage.io.imsave(filepath, faded, check_contrast=False)

    # ffmpeg requires a list of images with sequential filenames starting
    # at 0 for number wildcard
    files = getFilesInDirectory(directory)
    directory = './tmp/'

    for i, f in enumerate(files):
        image = skimage.io.imread(f, as_gray=False)
        filepath = os.path.join(directory, f'marching-squares-demo-{i:04d}.png')
        skimage.io.imsave(filepath, image, check_contrast=False)
    """

    # Export a video using ffmpeg
    # The options requirements that create a file that is playable in firefox
    # are quite spcific. E.g the quality flag can't be set to 0 (highest).
    source = os.path.join('tmp', r'marching-squares-demo-%04d.png')
    target = os.path.join('results','metaballs-animation.mp4')
    quality = 1
    framerate = samples_per_second
    movflag = '-movflags +faststart'
    command =  f'ffmpeg -framerate {framerate:d} -i {source} -c:v libx264 {movflag} -pix_fmt yuv420p -crf {quality:d} {target}'
    os.system(command)

# ==============================================================================
#
# Image series
#
# ==============================================================================

def makeImageSeries(image, iso, name):
    colors = Colors.colors
    w = image.shape[1]
    h = image.shape[0]

    mask = (image<iso).astype(np.uint8)

    polygons, vertices, _, _ = MarchingSquares.marchingSquares(image, iso, interpolate=True)
    polygons, vertices_int, _, _ = MarchingSquares.marchingSquares(image, iso, interpolate=False)

    dpi = 100

    fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    ax = fig.gca()
    plt.imshow(image, cmap='viridis')
    ax.set_axis_off()
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    filepath = os.path.join('./results', f'{name}-0.png')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    #plt.show()
    plt.close()

    fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    ax = fig.gca()
    plt.imshow(mask, cmap='gray')
    ax.set_axis_off()
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    filepath = os.path.join('./results', f'{name}-1.png')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    #plt.show()
    plt.close()

    fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    ax = fig.gca()
    options = {'draw-polygon': True, 'draw-faces': True, 'z-offset': 4, 'width': 4.0}
    DrawPolygon.filledLines(vertices, polygons[0], color=colors['blue'], fig=fig, options=options)
    ax.set_axis_off()
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    filepath = os.path.join('./results', f'{name}-2.png')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    #plt.show()
    plt.close()

    fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    ax = fig.gca()
    options = {'draw-polygon': True, 'draw-faces': True, 'z-offset': 4, 'width': 4.0}
    DrawPolygon.filledLines(vertices_int, polygons[0], color=colors['blue'], fig=fig, options=options)
    ax.set_axis_off()
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    filepath = os.path.join('./results', f'{name}-3.png')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    #plt.show()
    plt.close()

def makeMultiColoredImage(image):
    w = image.shape[1]
    h = image.shape[0]
    Colors.initRandomColor()

    image *= 10
    image = skimage.filters.gaussian(image, sigma=0.5)

    iso = 1

    polygons, vertices, _, _ = MarchingSquares.marchingSquares(image, iso, interpolate=False)

    dpi = 100

    fig = plt.figure(figsize=((w/h)*800/dpi, 800/dpi), dpi=dpi)

    options = {'draw-polygon': True, 'draw-faces': True, 'z-offset': 4, 'width': 4.0}
    for p in polygons:
        color = Colors.randomColor()
        DrawPolygon.filledLines(vertices, p, color, edgecolor='black', fig=fig, options=options)

    MakeIllustrations.drawImageGrid(image, draw_grid=False, fig=fig)

    ax = fig.gca()
    ax.set_axis_off()
    ax.set(xlim=(-1, w), ylim=(-1, h))
    ax.invert_yaxis()
    filepath = os.path.join('./results', f'example.png')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    filepath = os.path.join('./results', f'example.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, transparent=True)
    plt.show()
    plt.close()

def makePaddedExample(image):
    w = image.shape[1]
    h = image.shape[0]
    Colors.initRandomColor()

    image *= 10
    image = skimage.filters.gaussian(image, sigma=0.5)

    iso = 1

    polygons = 2*[[]]
    vertices = 2*[[]]

    polygons[0], vertices[0], _, _ = MarchingSquares.marchingSquares(image, iso, interpolate=False, pad=False)
    polygons[1], vertices[1], _, _ = MarchingSquares.marchingSquares(image, iso, interpolate=False, pad=True)

    name = ['non-padded', 'padded']

    for i in range(2):

        dpi = 100

        fig = plt.figure(figsize=((w/h)*800/dpi, 800/dpi), dpi=dpi)

        options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 4.0}
        for p in polygons[i]:
            if i==1:
                options['draw-faces'] = True
            color = Colors.randomColor()
            DrawPolygon.filledLines(vertices[i], p, color, edgecolor='black', fig=fig, options=options)

        ax = fig.gca()
        ax.set_axis_off()
        ax.set(xlim=(-2, w+1), ylim=(-2, h+1))
        ax.invert_yaxis()
        filepath = os.path.join('./results', f'{name[i]}.png')
        plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
        plt.show()
        plt.close()

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    print("Will make figures.")
    #Util.mkdir(['./',  'img'])

    img = skimage.io.imread('./img/bunny-mask-small.png', as_gray=True)
    #img = skimage.io.imread('./img/face.png', as_gray=True)
    #img = skimage.io.imread('./img/blobb-mask.png', as_gray=True)
    #img = skimage.io.imread('./img/box-mask.png', as_gray=True)
    #img = skimage.io.imread('./img/dot-mask.png', as_gray=True)
    iso = 0

    img *= 10
    img = skimage.filters.gaussian(img, sigma=1)

    iso = 6

    #makeImageSeries(img, iso, 'bunny')

    img = skimage.io.imread('./img/example.png', as_gray=True)
    makeMultiColoredImage(img)
    sys.exit()

    #animateMetaballsImages()
    #sys.exit()


    img = skimage.io.imread('./img/pad-example.png', as_gray=True)
    makePaddedExample(img)
    sys.exit()
