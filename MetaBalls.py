#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-04-XX
license: MIT

"""

import numpy as np

class MetaBalls:
    """
        Metaballs in 2D. Contains the position of a few metaballs that can change over time
        and an image grid to sample the metaballs summation.
        https://en.wikipedia.org/wiki/Metaballs
    """

    def __init__(self, n):
        """
            Create a uniform 2D grid of the domain x = [0, 1], y = [0, 1].
            Initialize a set of balls at random positions and with random velocities.
            Note: The resulting marching squares vertices are in the image domain and
                  not rescaled to the unit square.
                  As a result the image domain and its axes will be 200x200 for
                  a stepsize of 0.005 when creating the grid.
        """
        x = y = np.arange(0.0, 1.0, 0.005)
        X, Y = np.meshgrid(x, y)
        self.grid_X = X
        self.grid_Y = Y
        self.dt = 0.025
        self.metaballs = []
        self.velocities = []
        for i in range(n):
            self.metaballs.append(np.random.random(2))
            self.velocities.append( 0.1 * (np.random.random(2)-0.5) )

    def eval(self):
        """
            Sample the sum of all the metaballs on the grid and return the result.
            The result is essentially a 2D image.
        """
        X = self.grid_X
        Y = self.grid_Y
        Z = np.zeros(self.grid_X.shape)
        for ball in self.metaballs:
            x = ball[0]
            y = ball[1]
            Z += 1.0 / np.sqrt( (X-x)**2 + (Y-y)**2 )
        return Z

    def animate(self):
        """
            Time step the metaballs. Each ball have a velocity and they act on
            each other with both an attractive and a repulsive force that drives
            the system and creates and interesting animation.
            The balls also bounce on the walls of the domain.
            The timestep and grid size are essentially arbitrarily chosen so
            that they match the number of frames used when saving the results of
            the animation so that it looks pleasing.
        """
        dt = self.dt
        for i, ball in enumerate(self.metaballs):

            x = ball[0]
            y = ball[1]
            f = np.zeros(2)
            for j, other in enumerate(self.metaballs):
                if j==i:
                    continue
                r = ball-other
                d = np.sqrt(np.sum(r**2))
                f +=  - r * d**2 + r * d**3

            self.velocities[i] += dt*f
            v = self.velocities[i]

            x = ball[0] + dt*v[0]
            y = ball[1] + dt*v[1]

            if x < 0.0:
                x = 0.0
                self.velocities[i][0] = -self.velocities[i][0]
            if x > 1.0:
                x = 1.0
                self.velocities[i][0] = -self.velocities[i][0]
            if y < 0.0:
                y = 0.0
                self.velocities[i][1] = -self.velocities[i][1]
            if y > 1.0:
                y = 1.0
                self.velocities[i][1] = -self.velocities[i][1]
            ball[0] = x
            ball[1] = y
