#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-05-31
license: MIT

"""

import numpy as np
from scipy import sparse

# ==============================================================================
#
# Convert from and to flat image indices.
#
# ==============================================================================

def toFlatIndex(image, r, c):
    return image.shape[1]*r + c

def fromFlatIndex(image, index):
    r = index // image.shape[1]
    c = index % image.shape[1]
    return (r, c)

# ==============================================================================
#
# Topological edge connections and vertex interpolation for creating polygons.
#
# ==============================================================================

def extractPolygons(edges):
    """
        Extract connected polygonal chains from set of edges.
        The polygonal chains are grown in both directions from one starting edge,
        appending och prepending edges as they are encountered while traversing
        the list of unconnected edges.
        The head and tail vertex indices are updated as the chain is grown until
        no more edges to connect are left.
        A completed polygonal chain is stored in a list of such polygons.
    """
    polygons = []
    while edges:
        chain = []
        e = edges.pop()
        chain.append(e)
        tail = e[0]
        head = e[1]
        cont = True
        while cont and edges:
            e = None
            index = -1
            for i, ne in enumerate(edges):
                t = ne[0]
                h = ne[1]
                if head == t:
                    e = ne
                    chain.append(e)
                    head = h
                    index = i
                    break
                elif tail == h:
                    e = ne
                    chain.insert(0, e)
                    tail = t
                    index = i
                    break
                elif head == h:
                    e = ne
                    chain.append(e)
                    head = t
                    index = i
                    break
                elif tail == t:
                    e = ne
                    chain.insert(0, e)
                    tail = h
                    index = i
                    break
            if e is not None:
                edges.pop(index)
            else:
                cont = False
        polygons.append(chain)
    return polygons

def interpolateVertices(iso, p0, p1, v0, v1, interp=True):
    """
        Interpolate between two vertices in image coordinates.
        If binary is True the interpolated becomes equally weighted
        so the resulting vertex lies precisely in between p0 and p1.
        Values are converted to float before comparison as arithmetics of them
        keep the type so e.g. uint8 can under or overflow.
    """

    v0 = float(v0)
    v1 = float(v1)

    eps = 1.e-8
    if abs(v0-v1) < eps or not interp:
        return 0.5 * (p0+p1)

    if abs(v0 - iso) < eps:
        return p0
    elif abs(v1 - iso) < eps:
        return p1

    t = (iso - v0) / (v1 - v0)
    p = np.array([0, 0], dtype=np.float)
    p[0] = p0[0] + t * (p1[0] - p0[0])
    p[1] = p0[1] + t * (p1[1] - p0[1])
    return p

# ==============================================================================
#
# Marching Squares
#
# ==============================================================================

def marchingSquares(image, iso, interpolate=True, pad=True):
    """
        The indexed marching squares

        https://en.wikipedia.org/wiki/Marching_squares
        https://prideout.net/marching-squares
    """

    mask = (image <= iso).astype(np.uint8)
    if pad:
        large_mask = np.ones(np.array(mask.shape, dtype=int) + 2, dtype=np.uint8)
        large_mask[1:-1, 1:-1] = mask
        mask = large_mask

        large_image = np.ones(np.array(image.shape, dtype=int) + 2, dtype=image.dtype)
        large_image[1:-1, 1:-1] = image
        image = large_image

    verts_per_cell = 2*np.ones(16)
    verts_per_cell[5] = 4
    verts_per_cell[10] = 4
    verts_per_cell[0] = 0
    verts_per_cell[15] = 0

    verts_in_cell = [None]*16
    verts_in_cell[0] =  []
    verts_in_cell[1] =  [0, 3]
    verts_in_cell[2] =  [1, 0]
    verts_in_cell[3] =  [1, 3]

    verts_in_cell[4] =  [1, 2]
    verts_in_cell[5] =  [2, 3,
                         0, 2]
    verts_in_cell[6] =  [2, 0]
    verts_in_cell[7] =  [2, 3]

    verts_in_cell[8] =  [3, 2]
    verts_in_cell[9] =  [0, 2]
    verts_in_cell[10] = [3, 0,
                         1, 2]
    verts_in_cell[11] = [1, 2]

    verts_in_cell[12] = [3, 1]
    verts_in_cell[13] = [0, 1]
    verts_in_cell[14] = [3, 0]
    verts_in_cell[15] = []

    edges_in_cell = [None]*4
    edges_in_cell[0] = [0, 1]
    edges_in_cell[1] = [1, 2]
    edges_in_cell[2] = [2, 3]
    edges_in_cell[3] = [3, 0]

    cell_vertex_offsets = [None]*4
    cell_vertex_offsets[0] = [1, 0]
    cell_vertex_offsets[1] = [1, 1]
    cell_vertex_offsets[2] = [0, 1]
    cell_vertex_offsets[3] = [0, 0]

    nr_of_vertices = 0
    nr_of_edges = 0

    shape = np.array(mask.shape)
    shape -= 1

    nr_pixels = mask.size
    vertex_number = 0
    vertex_indices = []
    vertex_positions = []
    edges = []
    vertex_table = sparse.lil_matrix((nr_pixels, nr_pixels))

    # Assign values to grid depending on the binary mask of thresholded pixels
    grid = -1*np.ones(shape, dtype=np.uint8)
    for r in range(grid.shape[0]):
        for c in range(grid.shape[1]):
            grid[r,c] = (mask[r,c]<<3) | (mask[r,c+1]<<2) |\
                        (mask[r+1,c]<<0) | (mask[r+1,c+1]<<1)
            nr_of_vertices += verts_per_cell[grid[r,c]]
    nr_of_edges = nr_of_vertices // 2

    # Find the vertices as pairs of indexed positions along the cell grid latice edges.
    offset = 0
    for r in range(grid.shape[0]):
        for c in range(grid.shape[1]):
            nr_verts = len(verts_in_cell[grid[r,c]])
            ei = 0
            edge = [0, 0]
            for i in range(0, len(verts_in_cell[grid[r,c]])):
                offset = np.array([r, c])
                v_index = verts_in_cell[grid[r,c]][i]
                ce = edges_in_cell[v_index]

                cvo0 = cell_vertex_offsets[ce[0]]
                cvo1 = cell_vertex_offsets[ce[1]]
                p0 = offset + cvo0
                p1 = offset + cvo1
                pp0 = p0
                pp1 = p1
                p0 = toFlatIndex(image, p0[0], p0[1])
                p1 = toFlatIndex(image, p1[0], p1[1])

                if p1<p0:
                    p0, p1 = p1, p0
                    pp0, pp1 = pp1, pp0

                vertex_index = 0
                if vertex_table[p0, p1] == 0:
                    vertex_index = vertex_number + 1
                    vertex_table[p0, p1] = vertex_index
                    vertex_indices.append([p0, p1])
                    vertex_number += 1
                else:
                    vertex_index = vertex_table[p0, p1]
                vertex_index -= 1 # Values stored in sparse matrix offset by 1

                edge[ei] = vertex_index
                ei = (ei+1) % 2
                if ei == 0:
                    edges.append(np.array([edge[0], edge[1]], dtype=int))

    # Find the physical (interpolated) positions of all vertices
    # Note that here we go from image coordinates which are row, column,
    # i.e. y, x to physical coordinates with order x, y.
    for vi in vertex_indices:
        p0 = np.array(fromFlatIndex(image, vi[0]))
        p1 = np.array(fromFlatIndex(image, vi[1]))
        v0 = image[p0[0], p0[1]]
        v1 = image[p1[0], p1[1]]
        vp = interpolateVertices(iso, p0, p1, v0, v1, interpolate)
        vp[0], vp[1] = vp[1], vp[0]
        if pad:
            vp[0] -= 1.0
            vp[1] -= 1.0
        vertex_positions.append(vp)

    # Put vertices in array instead of list and connect polygons
    vertices = np.array(vertex_positions)
    polygons = extractPolygons(edges)

    grid = grid[1:-1, 1:-1]
    return polygons, vertices, vertex_indices, grid
