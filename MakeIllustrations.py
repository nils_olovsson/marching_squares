#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-05-XX
license: MIT

"""

import os

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects

import skimage.io
import skimage.filters

import MarchingSquares
import DrawPolygon
import Colors

# ==============================================================================
#
# Individual drawing functions
#
# ==============================================================================

def drawImageGrid(image, draw_grid=True, fig=None):

    w = image.shape[0]
    h = image.shape[1]

    verts = np.zeros([(w+h+2)*2, 2], dtype=float)
    edges = np.zeros([w+h+2, 2], dtype=int)

    for i in range(0, 2*(h+1), 2):
        vpos = i//2
        verts[i  ,:] = np.array([vpos-0.5, 0-0.5], dtype=float)
        verts[i+1,:] = np.array([vpos-0.5, w-0.5], dtype=float)

    offset = 2*(h+1)
    for i in range(0, 2*(w+1), 2):
        vpos = i//2
        verts[offset + i  ,:] = np.array([0-0.5, vpos-0.5], dtype=float)
        verts[offset + i+1,:] = np.array([h-0.5, vpos-0.5], dtype=float)

    for i in range(h+1):
        index = 2*i
        edges[i,:] = np.array([index, index+1], dtype=int)

    voffset = 2*(h+1)
    offset = h+1
    for i in range(w+1):
        index = 2*i + voffset
        edges[offset+i,:] = np.array([index, index+1], dtype=int)
    grid_options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 1, 'width': 4.0}

    if not fig:
        dpi = 100
        fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    ax = fig.gca()

    if draw_grid:
        _, _, filled = DrawPolygon.filledLines(verts, edges, color='black', fig=fig, options=grid_options)
    else:
        a = 0
        b = 1
        c = 2*(h+1) - 1
        d = c + 2

        #print(verts[a])
        #print(verts[b])
        #print(verts[c])
        #print(verts[d])
        #print(verts[b-1])
        #print(verts[c-1])
        #print(verts[d-1])
        #print(verts[b+1])
        #print(verts[c+1])
        #print(verts[d+1])

        square_options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 3, 'width': 4.0}
        square = np.array([[a,b], [b,c], [c,d], [d,a]], dtype=int)
        _, _, filled = DrawPolygon.filledLines(verts, square, color='red', edgecolor='black', fig=fig, options=square_options)

    return fig

def drawImagePixelIndices(image, color):
    w = image.shape[1]
    h = image.shape[0]
    index = 0
    for y in range(h):
        for x in range(w):
            text = plt.text(x,y,str(index), fontsize=24, ha='center', va='center', fontweight='bold', color=color, zorder=10)
            text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
            index += 1
    return 0

def drawImageValues(image):

    w = image.shape[1]
    h = image.shape[0]
    for y in range(h):
        for x in range(w):
            v = image[y,x]
            text = plt.text(x,y,str(v), fontsize=36, ha='center', va='center', fontweight='bold', color='white')
            text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    plt.imshow(image, cmap='viridis')
    return 0

def drawImageMaskValues(image):
    w = image.shape[1]
    h = image.shape[0]
    for y in range(h):
        for x in range(w):
            v = image[y,x]
            #text = plt.text(x,y,str(v), fontsize=32, ha='center', va='center', fontweight='bold', color='white')
            #text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
            color = 'white'
            if v>1:
                color = 'black'
            plt.scatter(x, y, s=320, c=[color], marker='o', edgecolors='gray', linewidth=3, zorder=7)
    plt.imshow(image, cmap='viridis')
    return 0

def drawCellGrid(image, fig=None, draw_grid=True, draw_square=True):
    w = image.shape[1]
    h = image.shape[0]

    verts = np.zeros([(w+h)*2, 2], dtype=float)
    edges = np.zeros([w+h, 2], dtype=int)

    for i in range(0, 2*h, 2):
        vpos = i//2
        verts[i  ,:] = np.array([vpos-0.0, 0-0.0], dtype=float)
        verts[i+1,:] = np.array([vpos-0.0, w-1.0], dtype=float)
    #print(verts)
    #print("")

    offset = 2*h
    for i in range(0, 2*w, 2):
        vpos = i//2
        verts[offset + i  ,:] = np.array([0-0.0, vpos-0.0], dtype=float)
        verts[offset + i+1,:] = np.array([h-1.0, vpos-0.0], dtype=float)
    #print(verts)

    for i in range(h):
        index = 2*i
        edges[i,:] = np.array([index, index+1], dtype=int)

    voffset = 2*h
    offset = h
    for i in range(w):
        index = 2*i + voffset
        edges[offset+i,:] = np.array([index, index+1], dtype=int)

    grid_options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 4.0}
    if not fig:
        dpi = 100
        fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
        ax = fig.gca()
    if draw_grid:
        fig, _, filled = DrawPolygon.filledLines(verts, edges, color='red', edgecolor='gray', fig=fig, options=grid_options)

    a = 0
    b = 2*h-2
    c = 2*h-1
    d = 1

    colors = Colors.colors
    square_options = {'draw-polygon': not draw_grid, 'draw-faces': draw_square, 'z-offset': 3, 'width': 4.0}
    square = np.array([[a,b], [b,c], [c,d], [d,a]], dtype=int)
    fig, _, filled = DrawPolygon.filledLines(verts, square, color=colors['red'], edgecolor='gray', fig=fig, options=square_options)

    return fig

def drawCellGridConnections(image, fig, vertex_ind):
    w = image.shape[1]
    h = image.shape[0]
    verts = []
    for y in range(-1, h+1):
        for x in range(-1, w+1):
            verts.append([x, y])

    verts = np.array(verts)
    #vertex_ind -= 1

    #print(len(verts))
    #print(vertex_ind)
    colors = Colors.colors
    options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 3, 'width': 4.0}
    fig, _, filled = DrawPolygon.filledLines(verts, vertex_ind, color=colors['red'], edgecolor='gray', fig=fig, options=options)

    return fig

def drawCellGridValues(image, cells, fig):
    ax = fig.gca()
    w = cells.shape[1]
    h = cells.shape[0]
    for y in range(h):
        for x in range(w):
            v = cells[y,x]
            text = ax.text(x+0.5, y+0.5, str(v), fontsize=36, ha='center', va='center', fontweight='bold', color='white', zorder=6)
            text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    return 0

def drawPolygon(image, polygon, vertices, fig=None, enumerateedges=False, draw_faces=True, edge_color='black'):
    color_blue = '#CCCCFF'
    colors = Colors.colors
    options = {'draw-polygon': True, 'draw-faces': draw_faces, 'z-offset': 4, 'width': 4.0}
    if enumerateedges:
        options['enumerate-edges'] = True
    fig, _, _ = DrawPolygon.filledLines(vertices, polygon, color=colors['blue'], edgecolor=edge_color, fig=fig, options=options)

    return 0

def drawImageGridValues(image):
    return 0

# ==============================================================================
#
# Draw illustration of the marching squares steps
#
# ==============================================================================

def step1ImageValues(image):
    fig = drawImageGrid(image)
    drawImageValues(image)

    ax = fig.gca()
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()

    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step1.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step2Thresholded(image):
    fig = drawImageGrid(image)
    drawImageMaskValues(image)

    ax = fig.gca()
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()

    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step2.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step3CellOverlay(image):
    fig = drawImageGrid(image)
    fig = drawCellGrid(image, fig)
    drawImageMaskValues(image)

    ax = fig.gca()
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()

    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step3.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step4CellValues(image, cells):

    fig = drawImageGrid(image)
    fig = drawCellGrid(image, fig)
    drawImageMaskValues(image)
    drawCellGridValues(image, cells, fig)

    ax = fig.gca()
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()

    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step4.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step5Polygons(image, polygon, vertices, cells):
    fig = drawImageGrid(image)
    fig = drawCellGrid(image, fig)
    drawImageMaskValues(image)
    #drawCellGridValues(image, cells)
    drawPolygon(image, polygon, vertices, fig)

    ax = fig.gca()
    ax.scatter(vertices[:, 0],
               vertices[:, 1],
               s=120,
               zorder=7,
               marker='s',
               edgecolors='black',
               color='black')

    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step5.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step6Interpolate(image, polygon, vertices, cells, vertex_ind):
    fig = drawImageGrid(image)
    fig = drawCellGrid(image, fig=fig, draw_grid=False)
    fig = drawCellGridConnections(image, fig, vertex_ind)
    drawImageMaskValues(image)
    #drawCellGridValues(image, cells)
    drawPolygon(image, polygon, vertices, fig)

    #print(cells)

    ax = fig.gca()
    ax.scatter(vertices[:, 0],
               vertices[:, 1],
               s=120,
               zorder=7,
               marker='s',
               edgecolors='black',
               color='black')
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step6.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step7PolygonExtraction(image, polygon, vertices, cells):
    fig = drawImageGrid(image, draw_grid=False)
    fig = drawCellGrid(image, fig=fig, draw_grid=False, draw_square=False)
    drawPolygon(image, polygon, vertices, fig, enumerateedges=True)

    ax = fig.gca()
    ax.scatter(vertices[:, 0],
               vertices[:, 1],
               s=120,
               zorder=7,
               marker='s',
               edgecolors='black',
               color='black')
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    #ax.invert_yaxis()
    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step7.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def step8Final(image, polygon, vertices, cells):
    fig = drawImageGrid(image, draw_grid=False)
    drawPolygon(image, polygon, vertices, fig, enumerateedges=False)

    ax = fig.gca()
    w = image.shape[1]
    h = image.shape[0]
    ax.set(xlim=(-1, h), ylim=(-1, w))
    ax.invert_yaxis()
    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step8.svg')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()
    return 0

def stepIndexing(image, polygon, vertices, cells, vertex_ind):
    w = image.shape[1]
    h = image.shape[0]
    dpi = 100
    fig = plt.figure(figsize=(2*800/dpi, 800/dpi), dpi=dpi)
    print(fig)
    ax = fig.gca()

    Colors.colors

    colors = {}
    colors['vertex'] = 'tab:red'#Colors.colors['red']
    colors['edge']   = 'tab:blue'#Colors.colors['']
    colors['index']  = 'tab:olive'
    colors['table']  = 'tab:orange'

    #ax.invert_yaxis()
    drawImageGrid(image, True, fig)
    #fig = drawCellGrid(image, fig)
    #drawImageMaskValues(image)
    #drawCellGridValues(image, cells, fig)
    #drawPolygon(image, polygon, vertices, fig, draw_faces=False, edge_color=colors['edge'])

    options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 8.0}
    DrawPolygon.filledLines(vertices, polygon, color='black', edgecolor=colors['edge'], fig=fig, options=options)

    drawImagePixelIndices(image, colors['index'])
    fig = drawCellGridConnections(image, fig, vertex_ind)

    ax = fig.gca()
    ax.scatter(vertices[0:2, 0],
               vertices[0:2, 1],
               s=120,
               zorder=7,
               marker='s',
               edgecolors='black',
               color=colors['vertex'])

    # ----------------------------------------------------------------
    # Index
    # ----------------------------------------------------------------
    scale = 0.75

    offset = np.array([w+2, -1.5])
    p = offset + np.array([-1, 0.5*scale])
    text = plt.text(p[0] , p[1], "Image index", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['index'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    # ----------------------------------------------------------------
    # Vertex list
    # ----------------------------------------------------------------

    n = 8
    verts = np.zeros([n,2], dtype=float)
    edges = [] #np.zeros([9,2], dtype=int)

    offset = np.array([w+2, -0.5])
    for i in range(0,n,2):
        verts[i,  :] = np.array([0.5*scale*i, 0.00]) + offset
        verts[i+1,:] = np.array([0.5*scale*i, scale]) + offset

    edges.append([0,n-2])
    edges.append([1,n-1])
    edges.append([0,1])
    edges.append([2,3])
    edges.append([4,5])
    edges.append([6,7])
    edges = np.array(edges, dtype=int)
    options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 4.0}
    _, _, _ = DrawPolygon.filledLines(verts, edges, 'red', edgecolor=colors['vertex'], name='', fig=fig, options=options)


    p = offset + np.array([0.5*scale, 0.5*scale])
    text = plt.text(p[0] , p[1], "1, 6", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['index'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([0.5*scale*3, 0.5*scale])
    text = plt.text(p[0] , p[1], "5, 6", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['index'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([0.5*scale, 0.5*scale*2.75])
    text = plt.text(p[0] , p[1], "0", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([0.5*scale*3, 0.5*scale*2.75])
    text = plt.text(p[0] , p[1], "1", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([0.5*scale*5, 0.5*scale*2.75])
    text = plt.text(p[0] , p[1], "2", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([scale*(n//2 - 1)+0.33, 0.33*scale])
    text = plt.text(p[0] , p[1], "...", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([-1, 0.5*scale])
    text = plt.text(p[0] , p[1], "Vertices", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    # ----------------------------------------------------------------
    # Edge list
    # ----------------------------------------------------------------

    n = 8
    verts = np.zeros([n,2], dtype=float)
    edges = [] #np.zeros([9,2], dtype=int)

    offset = np.array([w+2, 1])
    for i in range(0,n,2):
        verts[i,  :] = np.array([0.5*scale*i, 0.00]) + offset
        verts[i+1,:] = np.array([0.5*scale*i, scale]) + offset

    edges.append([0,n-2])
    edges.append([1,n-1])
    edges.append([0,1])
    edges.append([2,3])
    edges.append([4,5])
    edges.append([6,7])
    edges = np.array(edges, dtype=int)
    options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 4.0}
    _, _, _ = DrawPolygon.filledLines(verts, edges, 'red', edgecolor=colors['edge'], name='', fig=fig, options=options)

    p = offset + np.array([0.5*scale, 0.5*scale])
    text = plt.text(p[0] , p[1], "0, 1", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    #p = offset + np.array([0.5*scale*3, 0.5*scale])
    #text = plt.text(p[0] , p[1], "2, 3", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    #text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([scale*(n//2 - 1)+0.33, 0.33*scale])
    text = plt.text(p[0] , p[1], "...", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['edge'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([-1, 0.5*scale])
    text = plt.text(p[0] , p[1], "Edges", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['edge'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    # ----------------------------------------------------------------
    # Table
    # ----------------------------------------------------------------

    n = 12
    verts = np.zeros([n,2], dtype=float)
    edges = [] #np.zeros([9,2], dtype=int)

    offset = np.array([w+2, 2])

    dx = scale
    dy = scale

    verts[0, :] = np.array([0.0*dx, 0.0*dy]) + offset
    verts[1, :] = np.array([2.0*dx, 0.0*dy]) + offset
    verts[2, :] = np.array([3.0*dx, 0.0*dy]) + offset

    verts[3, :] = np.array([0.0*dx, 1.0*dy]) + offset
    verts[4, :] = np.array([2.0*dx, 1.0*dy]) + offset
    verts[5, :] = np.array([3.0*dx, 1.0*dy]) + offset

    verts[6, :] = np.array([0.0*dx, 2.0*dy]) + offset
    verts[7, :] = np.array([2.0*dx, 2.0*dy]) + offset
    verts[8, :] = np.array([3.0*dx, 2.0*dy]) + offset

    verts[9, :] = np.array([0.0*dx, 3.0*dy]) + offset
    verts[10,:] = np.array([2.0*dx, 3.0*dy]) + offset
    verts[11,:] = np.array([3.0*dx, 3.0*dy]) + offset

    edges.append([0,2])
    edges.append([3,5])
    edges.append([6,8])
    edges.append([9,11])
    edges.append([0,9])
    edges.append([1,10])
    edges.append([2,11])
    edges = np.array(edges, dtype=int)
    options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 4, 'width': 4.0}
    _, _, _ = DrawPolygon.filledLines(verts, edges, 'red', edgecolor=colors['table'], name='', fig=fig, options=options)

    p = offset + np.array([dx, 0.5*dy])
    text = plt.text(p[0] , p[1], "1, 6", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['index'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([dx, 1.5*dy])
    text = plt.text(p[0] , p[1], "5, 6", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['index'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([2.5*dx, 0.5*dy])
    text = plt.text(p[0] , p[1], "0", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])
    
    p = offset + np.array([2.5*dx, 1.5*dy])
    text = plt.text(p[0] , p[1], "1", fontsize=24, ha='center', va='center', fontweight='bold', color=colors['vertex'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([1.5*dx, 3.5*dy])
    text = plt.text(p[0] , p[1], "...", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['table'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    p = offset + np.array([-1, 0.5*scale])
    text = plt.text(p[0] , p[1], "Table", fontsize=36, ha='center', va='center', fontweight='bold', color=colors['table'], zorder=10)
    text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

    # ----------------------------------------------------------------
    # Finalize axes and show
    # ----------------------------------------------------------------

    ax.set(xlim=(-1, 2*w+1), ylim=(-1, h))
    ax.invert_yaxis()
    plt.draw()
    directory = 'illustrations'
    filepath = os.path.join('.', directory, 'step-ind.svg')
    plt.savefig(filepath, pad_inches=0.0, bbox_inches='tight', facecolor=fig.get_facecolor(), edgecolor='black')
    print(f"Saved figure to: {filepath}")
    plt.show()
    plt.close()
    return 0

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.sans-serif": ["Times New Roman"]})

    print("Will make illustrations")
    image = np.array([[1, 1, 1, 1, 1],
                      [1, 2, 3, 2, 1],
                      [1, 3, 3, 3, 1],
                      [1, 2, 3, 2, 1],
                      [1, 1, 1, 1, 1]],dtype=np.uint8)
    iso = 1.5

    polygons, vertices_unint, vertex_indices, grid = MarchingSquares.marchingSquares(image, iso, interpolate=True)
    polygons, vertices_int, vertex_indices, grid = MarchingSquares.marchingSquares(image, iso, interpolate=False)

    #step1ImageValues(image)
    #step2Thresholded(image)
    #step3CellOverlay(image)
    #step4CellValues(image, grid)
    #step5Polygons(image, polygons[0], vertices_unint, grid)
    #step6Interpolate(image, polygons[0], vertices_int, grid, vertex_indices)
    #step7PolygonExtraction(image, polygons[0], vertices_int, grid)
    #step8Final(image, polygons[0], vertices_int, grid)
    stepIndexing(image, polygons[0], vertices_unint, grid, vertex_indices)
    print("Done!")
