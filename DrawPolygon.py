#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import os
import copy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.ticker as ticker

import matplotlib.patches as mpatches
import matplotlib.patheffects as path_effects

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
from matplotlib import animation

import ComputePolygon

import Util

#from random_color import initRandomColor, randomColor

# ----------------------------------------------------------
# Set global background colors for all plots so it fits
# publishing backgound.
# Usually #ffffff or a softer color #f5f5f0.
#
# Colors of indivual plots can be set by:
#   fig.patch.set_facecolor(__COLOR__)
#   ax = fig.gca()
#   ax.set_facecolor(__COLOR__)
# ----------------------------------------------------------
bkg_color = '#ffffff'
#bkg_color = '#ff00ff'
ax_color = bkg_color

color = {}
color['blue'] = '#CCCCFF'
color['red'] = '#FFAAAA'

plt.rcParams['figure.facecolor'] = bkg_color
plt.rcParams['axes.facecolor'] = ax_color

# ==============================================================================
#
# Draw polygon
#
# ==============================================================================

def drawNormalAndTangent(vertices, i, fig, options):
    """
    """
    n = len(vertices)
    j = i + 1
    if(i==n-1):
        j = 0
    p0 = vertices[i,:]
    p1 = vertices[j,:]

    l = 0.125
    hl = 0.02
    hw = 0.02
    dx = 0.5*(p1[0] - p0[0])
    dy = 0.5*(p1[1] - p0[1])

    t = (p1 - p0)
    t = (1.0/np.linalg.norm(t)) * t

    n = np.array([t[1], -t[0]])

    pc = 0.5 * (p0 + p1)
    dt = l * t
    plt.arrow(pc[0], pc[1], dt[0], dt[1], linewidth=4, head_width=hw, head_length=hl,  color='black', zorder=5.0)
    dn = l * n
    plt.arrow(pc[0], pc[1], dn[0], dn[1], linewidth=4, head_width=hw, head_length=hl,  color='black', zorder=5.0)


    plt.text(pc[0]+1.25*dt[0], pc[1]+1.25*dt[1], r"$\textbf{T}$", fontsize=28)
    plt.text(pc[0]+1.25*dn[0], pc[1]+1.25*dn[1], r"$\textbf{N}$", fontsize=28)

def drawArrowOnPolygon(p0, p1, ax, options):
    """
    """

    z_offset = 0
    if 'z-offset' in options.keys():
        z_offset = options['z-offset']

    hl = 0.02
    hw = 0.02
    hl = 0.1
    hw = 0.1
    dx = 0.5*(p1[0] - p0[0])
    dy = 0.5*(p1[1] - p0[1])
    plt.arrow(p0[0], p0[1], dx, dy, linewidth=2, head_width=hw, head_length=hl,  color='black', zorder=5.0+z_offset)

def defaultOptions():
    """
    """

    options = {}
    options['annotate-index-as'] = {}
    options['enumerate-edges'] = False
    options['draw-vertices'] = False
    options['draw-polygon'] = True
    options['draw-faces'] = True
    options['annotate-verts'] = False
    options['draw-edge-arrow'] = False
    options['continue'] = True
    options['save'] = False
    options['z-offset'] = 0
    options['width'] = 4.0

    return options

def filled(vertices, normals, color, arrow_positions=[], name='', fig=None, figsize=[12,12], options={}):
    """
    """

    def_options = defaultOptions()
    if not options:
        options = def_options
    else:
        for key in def_options:
            if not key in options.keys():
                options[key] = def_options[key]

    # Initialize the figure
    extents = ComputePolygon.extents(vertices)
    my_dpi=96
    #fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    if not fig:
        fig = plt.figure(figsize=figsize)
    ax = fig.gca()

    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)

    ax.axis('off')

    n,m = vertices.shape

    width = options['width']

    lines = []
    # Draw outer polygon
    if options['draw-polygon']:
        for i in range(0, n):
            j = i+1;
            if(i==n-1):
                j = 0
            p0 = vertices[i,:]
            p1 = vertices[j,:]
            l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
            l.set_color('black')
            l.set_linewidth(width)
            l.set_zorder( options['z-offset']+1)
            l.set_solid_capstyle('round')
            ax.add_line(l)
            lines.append(l)

    for i in arrow_positions:
        p0 = vertices[i,:]
        p1 = vertices[i+1,:]
        drawArrowOnPolygon(p0, p1, ax, options)

    # Draw vertices
    if options['draw-vertices']:
        ax.scatter(vertices[:, 0],
                   vertices[:, 1],
                   s=80,
                   zorder=3 + options['z-offset'],
                   edgecolors='black',
                   color='black')
    # Number vertices
    if options['annotate-verts']:
        for i in range(n):
            j = i+1;
            if(i==n-1):
                j = 0
            p0 = vertices[i-1,:]
            p1 = vertices[i,:]
            p2 = vertices[j,:]
            normal = normals[i,:]
            offset = 2*0.0175
            p = p1 + offset*normal + np.array([-0.02, -0.01])
            index = i
            if i in options['annotate-index-as'].keys():
                index = options['annotate-index-as'][i]
            plt.text(p[0], p[1], str(index), fontsize=24)

            #pn = p1 + offset*normal
            #l = mlines.Line2D([p1[0],pn[0]], [p1[1],pn[1]])
            #l.set_color('red')
            #l.set_linewidth(4)
            #l.set_solid_capstyle('round')
            #ax.add_line(l)

    # Draw filled polygon
    if options['draw-faces']:
        x = vertices[:,0]
        y = vertices[:,1]
        plt.fill(x, y, color=color, zorder=options['z-offset'])

    plt.gca().margins(0.01, 0.01)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())

    return fig, lines

def filledLineSoup(vertices, color, name='', fig=None, figsize=[12,12], options={}):
    """
    """

    def_options = defaultOptions()
    if not options:
        options = def_options
    else:
        for key in def_options:
            if not key in options.keys():
                options[key] = def_options[key]

    # Initialize the figure
    extents = ComputePolygon.extents(vertices)
    my_dpi=96
    #fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    if not fig:
        fig = plt.figure(figsize=figsize)
    ax = fig.gca()

    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)

    ax.axis('off')

    n, m = vertices.shape

    width = options['width']

    lines = []
    # Draw outer polygon
    if options['draw-polygon']:
        for i in range(0, n, 2):
            j = i+1;
            if(i==n-1):
                j = 0
            p0 = vertices[i,:]
            p1 = vertices[j,:]
            l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
            l.set_color('black')
            l.set_linewidth(width)
            l.set_zorder( options['z-offset']+1)
            l.set_solid_capstyle('round')
            ax.add_line(l)
            lines.append(l)

    # Draw vertices
    if options['draw-vertices']:
        ax.scatter(vertices[:, 0],
                   vertices[:, 1],
                   s=80,
                   zorder=3 + options['z-offset'],
                   edgecolors='black',
                   color='black')

    # Draw filled polygon
    #if options['draw-faces']:
    #    x = vertices[:,0]
    #    y = vertices[:,1]
    #    plt.fill(x, y, color=color, zorder=options['z-offset'])

    plt.gca().margins(0.01, 0.01)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())

    return fig, lines

def filledLines(vertices, edges, color, edgecolor='black', name='', fig=None, figsize=[12,12], options={}):
    """
    """

    def_options = defaultOptions()
    if not options:
        options = def_options
    else:
        for key in def_options:
            if not key in options.keys():
                options[key] = def_options[key]

    # Initialize the figure
    #extents = ComputePolygon.extents(vertices)
    my_dpi=96
    #fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    if not fig:
        fig = plt.figure(figsize=figsize)
    ax = fig.gca()

    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)

    ax.axis('off')

    n, m = vertices.shape

    width = options['width']

    lines = []
    # Draw outer polygon
    if options['draw-polygon']:
        for index, e in enumerate(edges):
            i = e[0]
            j = e[1]
            p0 = vertices[i,:]
            p1 = vertices[j,:]
            l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
            l.set_color(edgecolor)
            l.set_linewidth(width)
            l.set_zorder( options['z-offset']+2)
            l.set_solid_capstyle('round')
            l.set_solid_joinstyle('round')
            l.set_dash_capstyle('round')
            l.set_dash_joinstyle('round')
            #l.set_linestyle('--')
            ax.add_line(l)
            lines.append(l)

            if options['enumerate-edges']:
                x = 0.5*(p0[0] + p1[0])
                y = 0.5*(p0[1] + p1[1])
                #ax.text(x, y, nr)
                #index = len(edges)-(index+1)
                text = plt.text(x, y, str(index+1), fontsize=28, ha='center', va='center', fontweight='bold', color='white', zorder=6)
                text.set_path_effects([path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()])

            if options['draw-edge-arrow']:
                print('Draw edge arrow')
                drawArrowOnPolygon(p0, p1, ax, options)

    # Draw vertices
    if options['draw-vertices']:
        ax.scatter(vertices[:, 0],
                   vertices[:, 1],
                   s=80,
                   zorder=3 + options['z-offset'],
                   edgecolors='black',
                   color='black')

    # Draw filled polygon
    filled = []
    if options['draw-faces']:
        x = vertices[:,0]
        y = vertices[:,1]
        x = []
        y = []
        for e in edges:
            x0 = vertices[e[0]][0]
            y0 = vertices[e[0]][1]
            x.append(x0)
            y.append(y0)
        x.append(vertices[edges[0][0]][0])
        y.append(vertices[edges[0][0]][1])
        x = np.array(x)
        y = np.array(y)
        filled = plt.fill(x, y, color=color, zorder=options['z-offset']+1)

    plt.gca().margins(0.01, 0.01)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())

    return fig, lines, filled
