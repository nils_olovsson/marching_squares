#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import numpy as np

# ==============================================================================
#
# Compute values for polygon
#
# ==============================================================================

def extents(vertices):
    """
        Get the extents, axis aligned bounding box, of the set of vertices.
    """

    box = np.zeros([4,1])
    box[0] = np.min(vertices[:,0]) # left
    box[1] = np.min(vertices[:,1]) # bottom
    box[2] = np.max(vertices[:,0]) # right
    box[3] = np.max(vertices[:,1]) # top
    return box

def vertexNormal(v0, v1, v2):
    """
        Compute the 2D vertex normal, for placing rendered vertex numbers.
    """
    e0 = v1 - v0
    e1 = v2 - v1
    n0 = np.array([e0[1], -e0[0]])
    n1 = np.array([e1[1], -e1[0]])
    l0 = np.linalg.norm(e0)
    l1 = np.linalg.norm(e1)

    n = 0.5*(1.0/l0*n0 + 1.0/l1*n1)
    n = 1.0 / np.linalg.norm(n) * n
    return n

def normals(vertices):
    """
        Compute the 2D vertex normals of the polygon, for placing rendered
        vertex numbers.
    """
    n,m = vertices.shape
    normals = np.zeros(vertices.shape)
    for i in range(n):
        v0 = vertices[i-1,:]
        v1 = vertices[i,:]
        v2 = vertices[(i+1)%n,:]
        normals[i,:] = vertexNormal(v0,v1,v2)

    return normals

def tangents(vertices):
    """
        Compute the 2D vertex tangents of the polygon by computing the normal
        and rotating it 90 degrees counter clockwise.
    """
    n,m = vertices.shape
    tangents = np.zeros(vertices.shape)
    for i in range(n):
        i0 = i-1
        if i0 < 0:
            i0 = n - i - 1
        v0 = vertices[i0,:]
        v1 = vertices[i,:]
        v2 = vertices[(i+1)%n,:]

        normal = vertexNormal(v0,v1,v2)
        tangents[i, 0] = -normal[1]
        tangents[i, 1] =  normal[0]
        #tangents[i, 0] =  normal[1]
        #tangents[i, 1] = -normal[0]

    return tangents
