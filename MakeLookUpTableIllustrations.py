#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-05-31
license: MIT

"""

import os

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects

import skimage.io
import skimage.filters

import MarchingSquares
import DrawPolygon
import Colors

# ==============================================================================
#
# Main
#
# ==============================================================================

def makeLookUpTableIllustration(directory):
    """
    """

    colors = Colors.colors

    vertices = np.array([[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0],
                         [0.5, 0.0], [1.0, 0.5], [0.5, 1.0], [0.0, 0.5],
                         [0.5, 0.5], [0.40, 0.40], [0.60, 0.40], [0.60, 0.60], [0.40, 0.60]], dtype=float)
    box = np.array([[0,1], [1,2], [2,3], [3,0]], dtype=int)

    def addCase(fig, nr, offset, values, verts, polygons, edges, label=''):
        """
            Add one of the 16 cases to the 4x4 grid layout. Each case is
            determined by the binary values at one of the corners.
            Polygons that indicate the area inside of the iso band are manually
            defined as are the edges that are added at the different cases.
            Exameple: 0---------1
                      |       \ |
                      |        \|
                      |         |
                      0---------0
                        Case 4
        """

        # Copy vertices and add offsets to place case at grid position
        r, c = verts.shape
        verts = verts.copy()
        for i in range(r):
            verts[i,:] += offset

        # Draw vertex at pixel dot
        for i, v in enumerate(values):
            if i>3:
                i = 8
            color = 'white'
            if v==0:
                color = 'black'
            plt.scatter(verts[i][0], verts[i][1], s=180, c=[color], marker='o', edgecolors='gray', linewidth=3, zorder=7)

        # Draw cell box
        cell_options = {'draw-polygon': True, 'draw-faces': True, 'z-offset': 3, 'width': 4.0}
        fig, _, filled = DrawPolygon.filledLines(verts, box, color=colors['red'], edgecolor='gray', fig=fig, options=cell_options)

        # Draw text
        pos = 0.5 * (verts[0] + verts[1]) - np.array([0.0, 0.25], dtype=float)
        if not label:
            label = f"Case {nr}"
        else:
            label = f"Case {label}"
        label = "\\textbf{" + label + "}"
        text = plt.text(pos[0], pos[1], label, fontsize=16, ha='center', va='center', fontweight='bold', color='black', zorder=6)
        #text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'), path_effects.Normal()])

        # Draw edges
        if edges is not None:
            edge_options = {'draw-polygon': True, 'draw-faces': False, 'z-offset': 3, 'width': 4.0, 'draw-edge-arrow': True}
            fig, _, filled = DrawPolygon.filledLines(verts, edges, color=colors['red'], edgecolor='black', fig=fig, options=edge_options)

            for e in edges:
                x = [verts[e[0]][0], verts[e[1]][0]]
                y = [verts[e[0]][1], verts[e[1]][1]]
                plt.scatter(x, y, s=60, c=['black'], marker='s', edgecolors='black', linewidth=1, zorder=7)

        # Draw polygons
        for poly in polygons:
            poly_options = {'draw-polygon': False, 'draw-faces': True, 'z-offset': 4, 'width': 4.0}
            fig, _, filled = DrawPolygon.filledLines(verts, poly, color=colors['blue'], edgecolor='black', fig=fig, options=poly_options)

    # ----------------------------------------------------------------
    # Create a figure for the 16 different cases
    # ----------------------------------------------------------------

    # Offsets that puts the cases into a 4x4 grid
    offsets = []
    offsets.append(np.array([0.0, 4.5], dtype=float))
    offsets.append(np.array([1.5, 4.5], dtype=float))
    offsets.append(np.array([3.0, 4.5], dtype=float))
    offsets.append(np.array([4.5, 4.5], dtype=float))

    offsets.append(np.array([0.0, 3.0], dtype=float))
    offsets.append(np.array([1.5, 3.0], dtype=float))
    offsets.append(np.array([3.0, 3.0], dtype=float))
    offsets.append(np.array([4.5, 3.0], dtype=float))

    offsets.append(np.array([0.0, 1.5], dtype=float))
    offsets.append(np.array([1.5, 1.5], dtype=float))
    offsets.append(np.array([3.0, 1.5], dtype=float))
    offsets.append(np.array([4.5, 1.5], dtype=float))

    offsets.append(np.array([0.0, 0.0], dtype=float))
    offsets.append(np.array([1.5, 0.0], dtype=float))
    offsets.append(np.array([3.0, 0.0], dtype=float))
    offsets.append(np.array([4.5, 0.0], dtype=float))

    # Values
    values = [[]]*16
    values[0]  = [0,0,0,0]
    values[1]  = [1,0,0,0]
    values[2]  = [0,1,0,0]
    values[3]  = [1,1,0,0]

    values[4]  = [0,0,1,0]
    values[5]  = [1,0,1,0]
    values[6]  = [0,1,1,0]
    values[7]  = [1,1,1,0]

    values[8]  = [0,0,0,1]
    values[9]  = [1,0,0,1]
    values[10] = [0,1,0,1]
    values[11] = [1,1,0,1]

    values[12] = [0,0,1,1]
    values[13] = [1,0,1,1]
    values[14] = [0,1,1,1]
    values[15] = [1,1,1,1]

    # Edges
    edges = [None]*16
    edges[1]  = np.array([[7,4]], dtype=int)
    edges[2]  = np.array([[4,5]], dtype=int)
    edges[3]  = np.array([[7,5]], dtype=int)
    edges[4]  = np.array([[5,6]], dtype=int)
    edges[5]  = np.array([[5,4], [7,6]], dtype=int)
    edges[6]  = np.array([[4,6]], dtype=int)
    edges[7]  = np.array([[7,6]], dtype=int)
    edges[8]  = np.array([[6,7]], dtype=int)
    edges[9]  = np.array([[6,4]], dtype=int)
    edges[10] = np.array([[4,7], [6,5]], dtype=int)
    edges[11] = np.array([[6,5]], dtype=int)
    edges[12] = np.array([[5,7]], dtype=int)
    edges[13] = np.array([[5,4]], dtype=int)
    edges[14] = np.array([[4,7]], dtype=int)

    # Polygons
    polygons = [[]]*16
    polygons[1] = [np.array([[7,4], [4,1], [1,2], [2,3], [3,7]], dtype=int)]
    polygons[2] = [np.array([[4,5], [5,2], [2,3], [3,0], [0,4]], dtype=int)]
    polygons[3] = [np.array([[5,2], [2,3], [3,7], [7,5]], dtype=int)]

    polygons[4] = [np.array([[0,1], [1,5], [5,6], [6,3], [3,0]], dtype=int)]
    polygons[5] = [np.array([[6,3], [3,7], [7,6]], dtype=int), np.array([[4,1], [1,5], [5,4]], dtype=int)]
    polygons[6] = [np.array([[0,4], [4,6], [6,3], [3,0]], dtype=int)]
    polygons[7] = [np.array([[6,3], [3,7], [7,6]], dtype=int)]

    polygons[8]  = [np.array([[0,1], [1,2], [2,6], [6,7], [7,0]], dtype=int)]
    polygons[9]  = [np.array([[4,1], [1,2], [2,6], [6,4]], dtype=int)]
    polygons[10]  = [np.array([[0,4], [4,7], [7,0]], dtype=int), np.array([[5,2], [2,6], [6,5]], dtype=int)]
    polygons[11] = [np.array([[5,2], [2,6], [6,5]], dtype=int)]

    polygons[12] = [np.array([[0,1], [1,5], [5,7], [7,0]], dtype=int)]
    polygons[13] = [np.array([[4,1], [1,5], [5,4]], dtype=int)]
    polygons[14] = [np.array([[0,4], [4,7], [7,0]], dtype=int)]

    # Create and save figure of all 16 cases
    dpi = 100
    fig = plt.figure(figsize=(800/dpi, 800/dpi), dpi=dpi)
    for i in range(16):
        addCase(fig, i, offsets[i], values[i], vertices, polygons[i], edges[i])

    ax = fig.gca()
    ax.set(xlim=(-0.5, 4*1.5), ylim=(-0.5, 4*1.5))

    plt.draw()
    filepath = os.path.join('.', directory, 'cases.svg')
    #plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, transparent=True)
    print(f"Saved figure to: {filepath}")

    #plt.show()
    plt.close()

    # ----------------------------------------------------------------
    # Create a figure for the four special cases that solves ambiguity
    # ----------------------------------------------------------------

    # Offsets that puts the cases into a 4x4 grid
    offsets = []
    offsets.append(np.array([0.0, 1.5], dtype=float))
    offsets.append(np.array([1.5, 1.5], dtype=float))
    offsets.append(np.array([0.0, 0.0], dtype=float))
    offsets.append(np.array([1.5, 0.0], dtype=float))

    # Values
    values = [[]]*16
    values[0]  = [1,0,1,0,0]
    values[1]  = [0,1,0,1,0]
    values[2]  = [1,0,1,0,1]
    values[3]  = [0,1,0,1,1]

    # Edges
    edges = [None]*16
    edges[0] = np.array([[7,9], [9,4], [5,11], [11,6]], dtype=int)
    edges[1] = np.array([[6,12], [12,7], [4,10], [10,5]], dtype=int)
    edges[2] = np.array([[7,12], [12,6], [5,10], [10,4]], dtype=int)
    edges[3] = np.array([[4,9], [9,7], [6,11], [11,5]], dtype=int)

    # Polygons
    polygons = [[]]*16
    polygons[0] = [np.array([[4,1], [1,5], [5,11], [11,6], [6,3], [3,7], [7,9], [9,4]], dtype=int)]
    polygons[1] = [np.array([[0,4], [4,10], [10,5], [5,2], [2,6], [6,12], [12,7], [7,0]], dtype=int)]
    polygons[2] = [np.array([[7,12], [12,6], [6,3], [3,7]], dtype=int),
                   np.array([[4,1], [1,5], [5,10], [10,4]], dtype=int)]
    polygons[3] = [np.array([[0,4], [4,9], [9,7], [7,0]], dtype=int),
                   np.array([[5,2], [2,6], [6,11], [11,5]], dtype=int)]

    labels = ['5A', '10A', '5B', '10B']

    # Create and save figure of all 16 cases
    s = (2*1.5+0.5) / (4*1.5+0.5)
    dpi = 100
    fig = plt.figure(figsize=(s*800/dpi, s*800/dpi), dpi=dpi)
    for i in range(4):
        addCase(fig, i, offsets[i], values[i], vertices, polygons[i], edges[i], labels[i])

    ax = fig.gca()
    ax.set(xlim=(-0.5, 2*1.5), ylim=(-0.5, 2*1.5))

    plt.draw()
    filepath = os.path.join('.', directory, 'ambiguity.svg')
    #plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, transparent=True)
    print(f"Saved figure to: {filepath}")
    #plt.show()
    plt.close()

def makeCellVertexOrderIllustration(directory):
    colors = Colors.colors

    vertices = np.array([[ 0.00,  0.00], [1.00, 0.00], [1.00, 1.00], [ 0.00, 1.00],
                         [-0.20, -0.20], [1.20,-0.20], [1.20, 1.20], [-0.20, 1.20]], dtype=float)
    box = np.array([[0,1], [1,2], [2,3], [3,0]], dtype=int)

    s = (2*1.5+0.5) / (1*1.5+0.5)
    dpi = 100
    fig = plt.figure(figsize=(s*100/dpi, s*100/dpi), dpi=dpi)

    print(vertices[0:4])

    # Draw vertex at pixel dot
    #for i, v in enumerate(values):
    plt.scatter(vertices[0:4,0], vertices[0:4,1], s=180, c='black', marker='o', edgecolors='gray', linewidth=3, zorder=7)
    # Draw cell box
    cell_options = {'draw-polygon': True, 'draw-faces': True, 'z-offset': 3, 'width': 4.0}
    fig, _, filled = DrawPolygon.filledLines(vertices, box, color=colors['red'], edgecolor='gray', fig=fig, options=cell_options)

    labels = ['A', 'B', 'C', 'D']
    for i in range(4):
        label = "\\textbf{" + labels[i] + "}"
        text = plt.text(vertices[4+i,0], vertices[4+i,1], label, fontsize=16, ha='center', va='center', fontweight='bold', color='black', zorder=6)

    ax = fig.gca()
    ax.set(xlim=(-0.25, 1.25), ylim=(-0.25, 1.25))
    plt.draw()
    #plt.show()
    filepath = os.path.join('.', directory, 'cell-vertex-order.svg')
    #plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, facecolor=fig.get_facecolor(), edgecolor='black')
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0.0, transparent=True)

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.sans-serif": ["Times New Roman"]})
    print("Will make illustration of the 15 cases")
    directory = 'illustrations'
    #makeLookUpTableIllustration(directory)
    makeCellVertexOrderIllustration(directory)
    print('Done!')
